
<h1 align="center">
  <img alt="Logo" title="Logo " src="assets/linux.png" width="200px" />
</h1>

# setup-linux-debian
> Base linux script to install theme programs and essential programming icons, with a greater focus on: Node.js, React.JS and React Native.

![Setup Linux](assets/demo.gif)

 ## Basic Programs <Mandatory>

 <ul>
  <li><a href="https://www.vim.org/download.php">Vim</a></li>
  <li><a href="https://curl.haxx.se/download.html">Curl</a></a></li>
  <li><a href="https://git-scm.com/download/linux"> Git </a></li>
 </ul>

## Optional Programs <You can choose via Whiptail>

<ul>
    <li> <a href="https://code.visualstudio.com/"> Vscode </a></li>
    <li> <a href="https://flameshot.js.org/#/">Flameshot</a></li>
    <li> <a href="https://www.docker.com/get-started"> Docker </a> </li>
    <li> <a href="https://docs.docker.com/compose/"> Docker Compose </a> </li>
    <li> <a href="https://dbeaver.io/">DBeaver</a></li>
    <li> <a href="https://github.com/nvm-sh/nvm"> Nvm </a></li>
    <li> <a href="https://yarnpkg.com/en/docs/getting-started">Yarn</a> </li>
    <li> <a href="https://github.com/infinitered/reactotron">Reactotron</a> </li>
    <li> <a href="https://discordapp.com/">Discord</a> </li>
    <li> <a href="https://github.com/phw/peek">Peek</a> </li>
    <li> <a href="https://github.com/vinceliuice/Canta-theme/">Canta Theme </a> </li>
    <li><a href="https://snapcraft.io/store"> Snapd </a>
      <ul>    
      <li> <a href="https://support.insomnia.rest/article/23-installation#ubuntu">Insomnia</a> </li>    
      <li> <a href="https://postman.com">Postman</a> </li>    
      <li> <a href="https://www.spotify.com/br/download/linux/"> Spotify </a> </li>    
      <li> <a href="https://slack.com/intl/pt-br/downloads/linux"> Slack </a> </li>    
    </ul>
    </li>
</ul>

 ## Maybe you like it <NOT in the script>
<ul>
  <li><a href="https://code.visualstudio.com/docs/editor/settings-sync">Settings Sync</a></li>
  <li><a href="https://draculatheme.com/">Dracula Theme</a></li>
  <li><a href="https://github.com/tonsky/FiraCode">Fira Code</a></li>
  <li><a href="https://www.notion.so/Configurando-o-Terminal-c3dcaf4c54a241228288b513c4e936b4">ZSH</a></li>
</ul>



## Vscode Extensions Tips <Settings Sync>
<ul>
  <li>Dracula Official</li>
  <li>Color Highlight</li>
  <li>Bracket Pair Colorizer</li>
  <li>Material Icon Theme</li> 
  <li>Live Server</li>    
  <li>Live Share</li>
  <li>DotENV</li>
  <li>EditorConfig for VS Code</li>
  <li>Project Manager</li>
  <li>GitLens — Git supercharged</li>
  <li>Docker</li>
  <li>Rocketseat React Native</li>
  <li>VS Code ES7 React/Redux/React-Native/JS snippets</li>
  <li>vscode-styled-components</li>
</ul>

## Running Script

After cloning or downloading zip repository and entering the folder and execute the following commands:
- Run **`chmod +x ./install.sh`**;
- Run **`./install.sh`**;

## Contribute

1. Make the _fork_ of the project
2. Create a _branch_ for your modification (`git checkout -b feature/fooBar`)
3. Make _commit_ (`git commit -m '[FEAT] - Estrutura inicial do projeto'`)
4. _Push_ (`git push origin feature/fooBar`)
5. Create a new _Pull Request_

## Comments

- **Snapd** **is not working** in version 20 of **Linux Mint** - Checked on **07/26/2020**
